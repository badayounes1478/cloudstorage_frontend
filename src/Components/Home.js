import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import './Home.css'

const Home = () => {

    const [videos, setVideos] = useState([])
    const [images, setImages] = useState([])

    useEffect(() => {
        Axios.get("https://apollo.yogved.in/apollo/video/all/videos").then(res => {
            let downloadedLinkAdded = res.data.map(data => {
                data.download = `https://apollo.yogved.in/apollo/video/download/${data._id}`
                return data
            })
            setVideos(downloadedLinkAdded)
        }).catch(err => {
            console.log(err)
        })


        Axios.get("https://muse.yogved.in/api/images/1").then(res => {
            let downloadedLinkAdded = res.data.map(data => {
                data.download = `https://muse.yogved.in/api/images/download/${data._id}`
                return data
            })
            setImages(downloadedLinkAdded)
        }).catch(err => {
            console.log(err)
        })

    }, [])

    return (
        <div className="container">
            <div>
                {
                    videos.map(data => {
                        if (data.status === "completed") {
                            return <div key={data._id} className="video-card">
                                <video width="320" height="240" controls src={data.url} />
                                <a href={data.download}>Download</a>
                            </div>
                        } else {
                            return null
                        }
                    })
                }
            </div>
            <div>
                {
                    images.map(data => {
                        return <div key={data._id} className="video-card">
                            <img alt={data._id} src={data.url} width="320" height="240" />
                            <a href={data.download}>Download</a>
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default Home
