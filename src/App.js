import React from 'react'
import {BrowserRouter, Switch, Route } from 'react-router-dom'
import './App.css'
import Home from './Components/Home'

const App = () => {
  return (
   <BrowserRouter>
     <Switch>
        <Route exact path="/" component={Home} />
     </Switch>
   </BrowserRouter>
  )
}

export default App
